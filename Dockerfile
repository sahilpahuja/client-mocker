FROM openjdk:8
COPY ./src /temp
WORKDIR /temp
RUN ["javac", "ClientRequestsManager.java"]
ENTRYPOINT ["java", "ClientRequestsManager"]