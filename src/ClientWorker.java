import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

public class ClientWorker extends Thread {
    private int port;
    private String host;
    private String route;
    private Socket socket;
    private PrintWriter output;
    private BufferedReader input;
    private Reader inputReader;
    private Writer inputWriter;
    ClientWorker(int port, String host, String route) throws IOException {
        this.port = port;
        this.host = host;
        this.route = route;
        this.socket = new Socket(this.host, this.port);
        initStreams(this.socket);
    }

    @Override
    public void run() {
//        startInputRead();
        startServerPing();
    }

    private void startInputRead(){
        inputReader = new Reader();
        inputReader.startReading();
    }

    private void startServerPing(){
        inputWriter = new Writer();
        inputWriter.startPing();
    }

    private void initStreams(Socket socket) throws IOException{
        this.output = new PrintWriter(socket.getOutputStream(), true);
        this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    private class Reader implements Runnable {
        private volatile boolean status;
        private final Thread reader;
        public Reader() {
            reader = new Thread(this);
        }
        @Override
        public void run(){
            try {
                while(status){
                    while(!input.ready() && status);
                    if(status){
                        String incomingPacket = input.readLine();
                        System.out.println(incomingPacket);
                    }
                }
            } catch(IOException e){
                System.out.println("Exception while reading input from server: "+e);
            }
        }

        public void startReading(){
            status = true;
            reader.start();
        }

        public void stopReading(){
            status = false;
        }
    }

    private class Writer {
        private volatile boolean status;
        Random random;
        Writer() {
            random = new Random();
        }
        public void run(){
            while (status) {
                try {
                    Thread.sleep(getRandomRange());
                    output.println(route);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        public void startPing(){
            status = true;
            this.run();
        }

        public void stopPing(){
            status = false;
        }
        private int getRandomRange() {
            return this.random.nextInt((500 - 100) + 1) + 100;
        }
    }
}
