import java.io.IOException;

public class ClientRequestsManager {
    ClientRequestsManager(
            int port,
            String host,
            int noOfSuccessHitClientWorkers,
            int noOfFailHitClientWorkers
    ) throws IOException {
        createClientWorkers(noOfSuccessHitClientWorkers, port, host, "/api/v1/hello");
        createClientWorkers(noOfFailHitClientWorkers, port, host, "/api/fail");
    }

    private void createClientWorkers(int count, int port, String host, String route) throws IOException {
        for (int i = 0; i < count; ++i) {
            System.out.println("Created Ping worker " + i + ", assigned route " + route);
            new ClientWorker(port, host, route).start();
        }
    }

    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        String host = args[1];
        int noOfSuccessHitClientWorkers = Integer.parseInt(args[2]);
        int noOfFailHitClientWorkers = Integer.parseInt(args[3]);

        try {
            new ClientRequestsManager(port, host, noOfSuccessHitClientWorkers, noOfFailHitClientWorkers);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
